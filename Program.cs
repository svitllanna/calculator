﻿using System;

namespace calculator
{
    public class Program
    {
        public static bool RomanNumValidation(string arg)
        {
            arg.ToCharArray();
            foreach (var item in arg)
            {
                if (RomanConverter.RomNum.ContainsKey(item) == false)
                {
                    return false;
                }
            }
            return true;
        }
        public static void Main(string[] args)
        {
            int firstNum = GetArg("Put first number ");
            int secondNum = GetArg("Put second number ");
            int result = GetResult(firstNum, secondNum);
            string roman = RomanConverter.Translate(result);
            Console.Write("Result: ");
            Console.WriteLine(roman);
            Console.WriteLine("Do you want to continue? (Y/N): ");
            string answerQuestion = Console.ReadLine();
            if (answerQuestion == "Y")
            {
              Main(args);  
            }
        }
        public static int GetArg(string message
    )
        {
            Console.Write(message);
            string value = Console.ReadLine();
            bool isArgValid = RomanNumValidation(value);
            if (isArgValid == false)
            {
                Console.WriteLine("Number is invalid");
                return GetArg(message);
            }

            return RomanConverter.ParseRomanNumberToArabic(value);
        }
        public static int GetResult(int first, int second)
        {
            Console.WriteLine("Select action: +, -, *, /");
            string action = Console.ReadLine();
            switch (action)
            {
                case "+":
                    return Calculator.Sum(first, second);
                case "-":
                    return Calculator.Substraction(first, second);
                case "*":
                    return Calculator.Multiplication(first, second);
                case "/":
                    return Calculator.Divide(first, second);
                default:
                    Console.WriteLine("Invalid action");
                    return GetResult(first, second);
            }
        }
    }
}

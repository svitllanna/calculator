using System.Text;
using System.Collections.Generic;

public partial class RomanConverter
{
    public static Dictionary<char, int> RomNum = new Dictionary<char, int>()
    {
        {'I', 1},
        {'V', 5},
        {'X', 10},
        {'L', 50},
        {'C', 100},
        {'D', 500},
        {'M', 1000}
    };
    public static Dictionary<int, string> IntNum = new Dictionary<int, string>
        {
            { 1000, "M" },
            { 900, "CM" },
            { 500, "D" },
            { 400, "CD" },
            { 100, "C" },
            { 90, "XC" },
            { 50, "L" },
            { 40, "XL" },
            { 10, "X" },
            { 9, "IX" },
            { 5, "V" },
            { 4, "IV" },
            { 1, "I" },
        };

    // Converts Romanian number inserted into Arabic number
    public static int ParseRomanNumberToArabic(string romanNumber)
    {
        int number = 0;
        for (int i = 0; i < romanNumber.Length; i++)
        {
            if (i + 1 < romanNumber.Length && RomNum[romanNumber[i]] < RomNum[romanNumber[i + 1]])
            {
                number -= RomNum[romanNumber[i]];
            }
            else
            {
                number += RomNum[romanNumber[i]];
            }
        }
        return number;
    }
    
    // Converts calculated result into Romanian number
    public static string Translate(int resultInRom)
    {
        var roman = new StringBuilder();

        foreach (var item in IntNum)
        {
            while (resultInRom >= item.Key)
            {
                roman.Append(item.Value);
                resultInRom -= item.Key;
            }
        }

        return roman.ToString();
    }

}
